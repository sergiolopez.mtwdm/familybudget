import React from "react";
import {Grid} from "semantic-ui-react";
import IUser from '../../app/modules/user';
import ConfigurationForm from './ConfigurationForm';


// interface IProps{
//     selectedUser:IUser|null,
//     users:IUser[],
//     editUser:boolean,
//     editUserEvent:(user:IUser|null)=> void
//     cancelEvent: ()=>void,
//     saveUserEvent: (user:IUser)=>void
// }

const ConfigurationDashboard = () =>{
    return(
        <ConfigurationForm 
        />
    );
};

export default ConfigurationDashboard;