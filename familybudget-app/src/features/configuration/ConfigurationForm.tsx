import React, {Fragment, useState, useEffect} from 'react';
import {Grid, Segment, Form, Button} from 'semantic-ui-react';


const ConfigurationForm = () => {

    // const handleInputChange = (event:any) =>{
    //     const {name, value} = event.target;
    //     // console.log(event.target);
    //     setUser({...user,[name]:value})
    // }

    // let label = user.id == 0 ? "New Family Member" : "Edit Family Member";

    return(
        <Fragment>
            <h1>System Configuration</h1>
            {/* <Segment clearing> */}
                <Form>
                    <p>Notifications</p>
                    <Form.Checkbox 
                        name="chkEmailRegisterExpenses"
                        label="Send an email notification when a user register expenses"
                    />
                    <Form.Checkbox 
                        name="chkSMSRegisterExpenses"
                        label="Send an SMS notification when a user register expenses"
                    />
                    <p>User register</p>
                    <Form.Checkbox 
                        name="chkAutoRegisterUsers"
                        label="Allow auto-register users"
                    />
                    <Form.Checkbox 
                        name="chkConfirmRegistrationEmail"
                        label="Confirm registration via email"
                    />
                    <p>System errors</p>
                    <Form.Input 
                        name="email" 
                        // onChange={handleInputChange}
                        // placeholder=""
                        value=""
                        label="Email to notify system errors"
                    />
                    <Form.Input 
                        name="mobilePhone" 
                        // onChange={handleInputChange}
                        // placeholder=""
                        value=""
                        label="Mobile Phone number to notify system errors"
                    />
                    <Button floated='right' positive type='submit' content='Save'/>
                    <Button floated='right' type='submit' content='Cancel'/>
                </Form>
            {/* </Segment> */}
        </Fragment>
    )
};

export default ConfigurationForm;