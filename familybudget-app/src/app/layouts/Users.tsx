import React, { Component, Fragment, useEffect, useState } from 'react';
import { Loader } from 'semantic-ui-react';
import UserDashboard from '../../features/users/UsersDashboard';
import IUser from '../modules/user';
import api from '../../api/api';

// interface IState{
//     users:IUser[],
//     loaded:boolean,
// }

const Users = () =>{
    const [selectedUser, setSelectedUser] = useState<IUser|null>(null);
    const [users, setUsers] = useState<IUser[]>([]);
    const [loaded, setLoaded] = useState<boolean>(false);
    const [editUser, setEditUser] = useState<boolean>(false);
    
    useEffect(() => {
        console.log("cargando");
        if(!loaded){
            api.User.list().then((users)=>{
                setUsers(users);
                setLoaded(true);
                console.log("llamando api");
            });
        }
    });

    const handleEditEvent = (user: IUser|null) => {
        console.log(user);
        setSelectedUser(user);
        setEditUser(true);
    };

    const handleCancelEvent= ()=>{
        setEditUser(false);
        setSelectedUser(null);
    }

    const handleSaveEvent = (user:IUser) => {
        if(user.id == 0){
            api.User.create(user).then((userResponse) =>{
                users.push(userResponse);
                
                setUsers(users);
                setSelectedUser(null);
                setEditUser(false);        
            });
        }else{
            api.User.update(user).then((userResponse) =>{
                
                let index = users.findIndex( u => u.id == user.id);
                users[index] = userResponse;

                setUsers(users);
                setSelectedUser(null);
                setEditUser(false);
            });
        }       
        console.log(user);
    }

    if(loaded==false){
        return(
            <Loader active inline="centered"/>
        );
    }

    return(
        <Fragment>
            <UserDashboard 
                editUser={editUser}
                editUserEvent={handleEditEvent}
                users={users}
                selectedUser={selectedUser}
                cancelEvent={handleCancelEvent}
                saveUserEvent={handleSaveEvent}
            />
        </Fragment>
    );
}

// export default class Users2 extends Component{
    
//     readonly state:IState = {
//         users : [],
//         loaded : false,
//     }

//     componentDidMount(){
//         api.User.list().then((users)=>{
//             this.setState({
//                 users: users,
//                 loaded: true,
//             });
//         });

//         // let users= [{id:1, firstName:'Sergio', lastName:'López', userName:'slopezfu', password:''}];
        
//         // this.setState({
//         //     users:users,
//         //     loaded:true,
//         // });
//     }

//     render(){

//         if(this.state.loaded==false){
//             return(
//                 <Loader active inline="centered"/>
//             );
//         }

//         return(
//             <Fragment>
//                 <UserDashboard users={this.state.users}/>
//             </Fragment>
//         );
//     }
// };

export default Users;