import React, { Component, Fragment, useEffect, useState } from 'react';
import { Loader } from 'semantic-ui-react';
import ConfigurationDashboard from '../../features/configuration/ConfigurationDashboard';

const Configuration = () =>{
    const [loaded, setLoaded] = useState<boolean>(false);

    useEffect(() => {
        console.log("cargando");
        // if(!loaded){
        //     api.User.list().then((users)=>{
        //         setUsers(users);
        //         setLoaded(true);
        //         console.log("llamando api");
        //     });
        // }
        setLoaded(true);
    });

    if(loaded==false){
        return(
            <Loader active inline="centered"/>
        );
    }

    return(
        <Fragment>
            <ConfigurationDashboard             
            />
        </Fragment>
    );
}

export default Configuration;