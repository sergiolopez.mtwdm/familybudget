using System.Collections.Generic;
using System.Threading.Tasks;
using FamilyBudget.Api.Services.Interfaces;
using FamilyBudget.Entities;
using Microsoft.AspNetCore.Mvc;

namespace FamilyBudget.Api.Controllers
{
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileService _profileService;

        public ProfileController(IProfileService profileService)
        {
            _profileService = profileService;
        }        

        [HttpGet]
        [Route("api/[Controller]")]
        public async Task<ActionResult<IEnumerable<Profile>>> GetAll(){
            var result = await _profileService.GetAll();
            return Ok(result);
        }

        [HttpGet]
        [Route("api/[Controller]/{id}")]
        public async Task<ActionResult<Profile>> GetById(int id){
            var result = await _profileService.GetById(id);
            return Ok(result);
        }

        [HttpPost]
        [Route("api/[Controller]")]
        public async Task<ActionResult<Profile>> Add([FromBody] Profile profile){
            await _profileService.Add(profile);
            return Created($"api/profile/{profile.Id}", profile);
        }
        [HttpPut]
        [Route("api/[Controller]")]
        public async Task<ActionResult<Profile>> Update([FromBody] Profile profile){
            await _profileService.Update(profile);
            return Created($"api/profile/{profile.Id}", profile);
        }

        [HttpDelete]
        [Route("api/[Controller]/{id}")]
        public async Task<ActionResult<bool>> Delete(int id){
            var result = await _profileService.Delete(id);
            return result;
        }
    }
}