using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using FamilyBudget.Entities;

namespace FamilyBudget.Api.Repository.Interfaces
{
    public interface IProfileRepository
    {        
        DbConnection DbConnection{get;}
        Task<IEnumerable<Profile>> GetAll();
        Task<Profile> GetById(int id);
        Task<Profile> Add(Profile profile);
        Task<Profile> Update(Profile profile);
        Task<bool> Delete(int id);
    }
}