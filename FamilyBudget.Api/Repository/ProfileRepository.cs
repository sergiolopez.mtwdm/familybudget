using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using FamilyBudget.Api.Repository.Interfaces;
using FamilyBudget.Entities;
using MySqlConnector;
using System.Linq;
using FamilyBudget.Api.DataAccess.Interfaces;
using System.Data.Common;

namespace FamilyBudget.Api.Repository
{
    public class ProfileRepository : IProfileRepository
    {
        private readonly IData _data;

        public DbConnection DbConnection => _data.DbConnection;

        public ProfileRepository(IData data)
        {
            _data = data;
        }

        public async Task<IEnumerable<Profile>> GetAll()
        {            
            // var parameters = new { Deleted = 0};            
            // string sql = "SELECT * FROM PROFILE WHERE DELETED = @Deleted";
            var template = new Profile { Deleted = false};
            var parameters = new DynamicParameters(template);
            string sql = "SELECT * FROM Profile WHERE Deleted = @Deleted";

            var result = await DbConnection.QueryAsync<Profile>(sql, parameters);
            // result = result.Where(parameters=> parameters.Deleted == false);

            return result;
        }

        public async Task<Profile> GetById(int id)
        {
            // var parameters = new {Id = id, Deleted = 0};
            // string sql = "SELECT * FROM PROFILE WHERE ID = @Id AND DELETED = @Deleted";

            var template = new Profile {Id = id, Deleted = false};
            var parameters = new DynamicParameters(template);
            string sql = "SELECT * FROM PROFILE WHERE ID = @Id AND DELETED = @Deleted";

            // var result = await _conn.QuerySingleOrDefaultAsync<Profile>(sql, parameters);            
            // // var result = await _conn.GetAsync<Profile>(id);
            // _conn.Close();
            // return result;

            var result = await DbConnection.QueryAsync<Profile>(sql, parameters);            
            var profile = result.FirstOrDefault();

            return profile;
        }

        public async Task<Profile> Add(Profile profile)
        {
            profile.Deleted = false;
            await DbConnection.InsertAsync<Profile>(profile);

            return profile;
        }
        public async Task<Profile> Update(Profile profile)
        {
            await DbConnection.UpdateAsync<Profile>(profile);

            return profile;
        }

        public async Task<bool> Delete(int id)
        {
            // var profile = await _conn.GetAsync<Profile>(id);
            var profile = await GetById(id);
            profile.Deleted = true;
            await DbConnection.UpdateAsync<Profile>(profile);

            return true;
        }
    }
}